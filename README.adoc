# Uberspace account info

Get https://uberspace.de/en/[uberspace] account infos, like price, registered domains etc.. and saves it into a csv file.

I have several accounts which are read out via list as environment variables. 

e.g.
[source,bash]
----
export UBERSPACE_USERNAME='["USERNAME1", "USERNAME2", "USERNAME3"]'
export UBERSPACE_PASSWORD='["PASSWORD1", "PASSWORD2", "PASSWORD3"]'
----

If there are no env variables defined the script asks for username and password.